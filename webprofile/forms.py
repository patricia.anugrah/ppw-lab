from django import forms

class JadwalForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    
    nama_kegiatan = forms.CharField(label='NAMA KEGIATAN', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateTimeField(label='TANGGAL/JAM', required=True, widget=forms.DateTimeInput(attrs={'type':'datetime-local', 'class':'form-control'}))
    tempat = forms.CharField(label='TEMPAT', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='KATEGORI', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    
