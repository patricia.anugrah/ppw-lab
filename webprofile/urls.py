# pages/urls.py
from django.urls import path

from .views import homePageView
from .views import jadwal_post

urlpatterns = [
    path('', homePageView, name='webprofile'),
    path('', jadwal_post, name='webprofile')
]