#importing required packages
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import JadwalForm
from .models import JadwalModel

from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
response = {'author': "Patricia Anugrah Setiani"}

def homePageView(request):
    response['JadwalForm'] = JadwalForm()
    return render(request, 'lab4.html', response)

def jadwal_post(request):
    form = JadwalForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['nama_kegiatan'] = request.POST['nama_kegiatan'] if request.POST['nama_kegiatan'] != "" else "Anonymous"
        response['tanggal'] = request.POST['tanggal'] if request.POST['tanggal'] != "" else "Anonymous"
        response['tempat'] = request.POST['tempat'] if request.POST['tempat'] != "" else "Anonymous"
        response['kategori'] = request.POST['kategori']
        message = Message(nama_kegiatan=response['nama_kegiatan'], tanggal=response['tanggal'], 
                          tempat=response['tempat'], kategori=response['kategori'])
        message.save()
    else:        
        print("INVALID!")
    response['JadwalForm'] = JadwalForm()
    html ='lab4.html'
    return render(request, html, response)

def message_table(request):
    message = JadwalForm.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html , response)

