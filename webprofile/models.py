from django.db import models

class JadwalModel(models.Model):
        nama_kegiatan = models.CharField(max_length=30)
        tanggal = models.DateTimeField(auto_now_add=True)
        tempat = models.CharField(max_length=30)
        kategori = models.CharField(max_length=30)

        def __str__(self):
            return self.nama_kegiatan
        
